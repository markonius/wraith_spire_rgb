﻿using System.Threading;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System;

namespace wraith_spire_rgb
{
	class Program
	{
		static void Main(string[] args)
		{
			var random = new Random();
			var tasks = new List<Task>();
			for (int i = 0; i < 32; i++)
			{
				tasks.Add(Task.Run(() => {
					while (true)
					{
						var target = random.NextDouble() * 5;
						Thread.Sleep((int)target * 1000);
						target = random.NextDouble() * 5;
						var stopwatch = Stopwatch.StartNew();
						while (stopwatch.Elapsed.TotalSeconds < target)
							Math.Log(1.3, 7);
					}
				}));
			}
			Console.WriteLine("Started tasks");

			foreach (var task in tasks)
				task.Wait();
			Console.WriteLine("Tasks done");
		}
	}
}
