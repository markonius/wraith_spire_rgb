#!/usr/bin/env python3

from cm_rgb.ctrl import LedChannel, LedMode, CMRGBController
import psutil
import subprocess
import re
import time
import atexit
from multiprocessing import Process


def hsv_to_rgb(h, s, v):
    if s == 0.0:
        v *= 255
        return (v, v, v)
    i = int(h*6.)  # XXX assume int() truncates!
    f = (h*6.)-i
    p, q, t = int(255*(v*(1.-s))), int(255*(v*(1.-s*f))
                                       ), int(255*(v*(1.-s*(1.-f))))
    v *= 255
    i %= 6
    if i == 0:
        return (v, t, p)
    if i == 1:
        return (q, v, p)
    if i == 2:
        return (p, v, t)
    if i == 3:
        return (p, q, v)
    if i == 4:
        return (t, p, v)
    if i == 5:
        return (v, p, q)


def do_it():
    try:
        time.sleep(5)
        c = CMRGBController()
        c.set_channel(LedChannel.R_SWIRL, LedMode.R_DEFAULT,
                      0xFF, 0xFF, 0xFF, 0xFF, 0x60)
        c.apply()

        now = time.time()
        while True:
            newNow = time.time()
            if newNow - now > 1:
                raise Exception("Probably slept, restarting...")
            now = newNow

            cpuUsage = psutil.cpu_percent(interval=0, percpu=False)
            usage = min(int(cpuUsage / 100 * 15), 15)
            # print("CPU Usage: %d" % cpuUsage, end=" ")

            sensors = subprocess.run(["sensors"], capture_output=True)
            sensors = sensors.stdout.decode("utf-8").split('\n')
            lscpu = subprocess.run(["lscpu"], capture_output=True)
            lscpu = lscpu.stdout.decode("utf-8").split('\n')

            matches = 0
            for l in sensors:
                if "Tdie" in l:
                    cpuTemp = int(next(re.finditer(r"[0-9]+", l)).group(0))
                    matches += 1
                elif "fan2" in l:
                    fanSpeed = int(next(re.finditer(r" [0-9]+", l)).group(0))
                    matches += 1

                if matches == 2:
                    break

            for l in lscpu:
                if "CPU MHz" in l:
                    cpuFreq = int(next(re.finditer(r" [0-9]+", l)).group(0))
                    break

            cpuTempV = 1.0 - min((cpuTemp - 20.0) / 45, 1.0)
            (cpuTempR, cpuTempG, cpuTempB) = hsv_to_rgb(cpuTempV, 1, 1)
            cpuTempV = 255 - int(cpuTempV * 255)
            # cpuTempR = cpuTempV
            # cpuTempB = 0  # 255 - cpuTempR
            # cpuTempG = 0  # cpuTempB // 2
            cpuTempS = 0 if cpuTemp < 60 else 0x1D
            # print("CPU Temperature: %d" % cpuTemp, end=" ")

            fanSpeedV = 1.0 - min(fanSpeed / 3200, 1.0)
            (fanSpeedR, fanSpeedG, fanSpeedB) = hsv_to_rgb(fanSpeedV, 1, 1)
            fanSpeedV = 255 - int(fanSpeedV * 255)
            # fanSpeedR = fanSpeedV
            # fanSpeedB = 255 - fanSpeedR
            # fanSpeedG = fanSpeedB // 2
            # print("Fan speed: %d" % fanSpeedV, end="\n")

            cpuFreqV = 1.0 - min((cpuFreq - 1000) / 3200, 1.0)
            (cpuFreqR, cpuFreqG, cpuFreqB) = hsv_to_rgb(cpuFreqV, 1, 1)
            cpuFreqV = 255 - int(cpuFreqV * 128)
            # print(str(cpuFreqV) + ":" + str(cpuFreq))

            c.set_channel(LedChannel.FAN, LedMode.STATIC, fanSpeedV,
                          fanSpeedR, fanSpeedG, fanSpeedB, 0x20)
            c.set_channel(LedChannel.R_BREATHE, LedMode.R_DEFAULT,
                          cpuTempV, cpuTempR, cpuTempG, cpuTempB, cpuTempS)
            c.set_channel(LedChannel.LOGO, LedMode.STATIC, cpuFreqV,
                          cpuFreqR, cpuFreqG, cpuFreqB, 0x20)

            ring_leds = ([LedChannel.R_BREATHE] * (15 - usage)) + \
                ([LedChannel.R_SWIRL] * usage)
            shift = -8
            ring_leds = ring_leds[-shift:]+ring_leds[:-shift]

            c.assign_leds_to_channels(
                LedChannel.LOGO, LedChannel.FAN, *ring_leds)
            c.apply()

            time.sleep(0.5)
    finally:
        c.restore()
        print("Restored state")


if __name__ == "__main__":
    while True:
        try:
            p = Process(target=do_it)
            p.start()
            p.join()
        except Exception as e:
            if not isinstance(e, KeyboardInterrupt):
                print(e)
                print("Restarting...")
            else:
                raise
