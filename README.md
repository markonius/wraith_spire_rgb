# Wraith System Monitor

## About

Changes Wraith cooler colors to display the following information in near real-time:

- **LOGO**: CPU frequency
- **FAN**: fan speed
- **RING**: CPU temperature (starts flashing when over 60°C)
- **RING FILL**: CPU usage

## How to use

- Install <https://github.com/gfduszynski/cm-rgb>
- Run SystemMonitorWithRGB.py on startup. You can use provided WraithSystemMonitor.service on Linux (systemd). C# project slams the CPU for testing.
